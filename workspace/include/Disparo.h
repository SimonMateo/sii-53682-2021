//Creado por simón mateo 11/11/2020

#pragma once

#include "ObjetoMovil.h"

class Disparo: public ObjetoMovil
{
public:
	Disparo(); //constructor
	virtual ~Disparo(); //destructor
	
	void Dibuja();
private:
};
