//creado por simon mateo 11/11/2020

#pragma once
#include "Disparo.h"
#include <vector>

class ListaDisparos
{
public:
	ListaDisparos();
	virtual ~ListaDisparos();
	
	bool Agregar(Disparo* disp);
	void Dibuja();
	void Mueve(float t);
	void EliminarDisparo(int ind);
	void DestruirContenido();
	
	int GetNum();
//private:
	std::vector<Disparo*> lista;
};
