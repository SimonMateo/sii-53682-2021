// Esfera.cpp: implementation of the Esfera class.
//Programa creado por Simón Mateo de Pedraza, a día ocho de octubre del año de nuestro señor de dosmil veinte
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
}

Esfera::~Esfera()
{

}

void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	//La esfera va reduciendo su tamaño según avanza el tiempo
	if (radio>0.1f)
	{
		radio-=t/10;
	}
	centro=centro + velocidad*t;
 }
