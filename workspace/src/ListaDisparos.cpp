#include "ListaDisparos.h"

ListaDisparos::ListaDisparos()
{

}

ListaDisparos::~ListaDisparos()
{

}

bool ListaDisparos::Agregar(Disparo* disp)
{
	lista.push_back(disp);
	return true;
}

void ListaDisparos::Dibuja()
{
	for(int i=0; i<lista.size(); i++)
	{
		lista[i]->Dibuja();
	}
}

void ListaDisparos::Mueve(float t)
{
	for (int i=0; i<lista.size(); i++)
	lista[i]->Mueve(t);
}

void ListaDisparos::EliminarDisparo(int ind)
{
	if(ind<0 || ind >=lista.size())return;
	
	delete lista[ind];
	lista.erase(lista.begin()+ind);
}

int ListaDisparos::GetNum(void)
{
	return lista.size();
}

void ListaDisparos::DestruirContenido()
{
	for (int i=0; i<lista.size(); i++)
	EliminarDisparo(i);
}
