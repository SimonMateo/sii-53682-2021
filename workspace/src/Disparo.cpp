//Creado por Simón Mateo a 11/11/2020

#include "Disparo.h"
#include "glut.h"

Disparo::Disparo()
{

}

Disparo::~Disparo()
{

}

void Disparo::Dibuja()
{
	glColor3ub(255,0,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(posicion.x,posicion.y,0);
	glutSolidSphere(0.2,15,15);
	glPopMatrix();
}
