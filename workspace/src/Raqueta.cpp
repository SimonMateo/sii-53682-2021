// Raqueta.cpp: implementation of the Raqueta class.
//Programa creado por Simón Mateo de Pedraza, a día ocho de octubre del año de nuestro señor de dosmil veinte
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1+=velocidad.y*t;
	y2+=velocidad.y*t;
}	
void Raqueta::Dispara(ListaDisparos* Disparos, float vel)
{
	Disparo* d=new Disparo();
	//Centro de la raqueta
	float centroX= (x1+x2)/2;
	float centroY= (y1+y2)/2;
	d->SetPos(centroX,centroY);
	d->SetVel(vel,0);	
	Disparos->Agregar(d);
}
