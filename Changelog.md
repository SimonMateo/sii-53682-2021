# Changelog
Todos los cambios relevantes a este proyecto se documentarán en este fichero
Este formato está basado en [keep a Changelog]

## [Unreleased]
## [1.2.1] - 2020-11-11
### Añadido
- Las plataformas ahora se mueven con los botones "w","s" (plataforma izquierda arriba y abajorespectivamente) y "o","l" (plataforma derecha, arriba y abajo respectivamente)
- Añadidos disparos que reducen el tamaño de la plataforma rival al impactar, los botones de disparo son "q" (plataforma izquierda) e "i" (plataforma derecha)

## [1.2.0] - 2020-10-22
### Añadido
- La pelota va disminuyendo de tamaño según avanza la partida, hasta un radio mínimo
-Cambio de estilo del Changelog

## [1.1.0] -2020-10-08
### Añadido
- Nuevo texto en la cabecera de los ficheros que identifica a Simón Mateo de Pedraza como autor del código.
