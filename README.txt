#README de las prácticas de Sistemas Informáticos Industriales
Por Simón Mateo de Pedraza

##Presentación del juego
Proyecto de juego de tenis para dos jugadores en línea local.

##Instrucciones
Para abrir el juego se debe ejecutar el archivo tenis
Al abrirlo, escoger una de las plataformas para controlarla
Se debe evitar que la pelota toque la pared detrás de tu pala o plataforma, para ello debes mover tu plataforma para que intercepte la pelota y rebote hacia tu rival
Puedes perjudicar a tu rival con disparos que empequeñecen su plataforma

##Controles
			Arriba	Abajo	Disparo
Plataforma izquierda:	w	s	q
Plataforma derecha:	o	l	i

Atención, si tienes el bloq. mayus. activo no funcionan, deben ser minúsculas.

##Agradecimientos


##Licencia
